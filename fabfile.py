# modul ini digunakan ketika proses deploy, tahapan2 instalasi pada host target ada pada script ini

from fabric.api import *
#from fabric.operations import local as lrun
from fabric.context_managers import settings

import os
import sys
import pymongo
import simplejson
import logging
from datetime import datetime, timedelta

# set currently directory dan load json data yg di produce oleh backend web server
CUR_PATH = os.path.abspath(os.path.dirname(__file__))
lookup = open(os.path.join(CUR_PATH, 'portal/deploying.json'), 'r').read()
lookup = simplejson.loads(lookup)

# semua variabel dibawah digunakan untuk mendefinisi keperluan deploying, dan data diambil dari object lookup hasil file json yg di read
username = lookup['username']
ip = lookup['ip']
port = lookup['port']
password = lookup['password']
label = lookup['label']
current_directory = os.path.abspath(os.path.dirname(__file__))

# function untuk konfigurasi environment host yg akan di deploy
def remote():
    env.hosts = ['%s@%s:%s' %(username,ip,port)] # username, ip dan port ssh target diset dalam bentuk list
    env.password = password # set password

# function ini berisi proses instalasi dan konfigurasi pada host target untuk keperluan monitoring
def copy():
    try:
        run('mkdir -p /home/%s/Desktop/monitoring/devel' % username)
        put(os.path.join(current_directory, 'source/psutil-0.4.1.tar.gz'), '/home/%s/Desktop/monitoring' % username)
        put(os.path.join(current_directory, 'source/web.py-0.36.tar.gz'), '/home/%s/Desktop/monitoring' % username)
        put(os.path.join(current_directory, 'source/devel/*'), '/home/%s/Desktop/monitoring/devel/' % username)
        put(os.path.join(current_directory, 'source/monitor.py'), '/home/%s/Desktop/monitoring' % username)
        put(os.path.join(current_directory, 'source/rest_service.py'), '/home/%s/Desktop/monitoring' % username)
        with cd('/home/%s/Desktop/monitoring' % username):
            run('echo "%s" > label.txt' % label)
            run('tar xzvf psutil-0.4.1.tar.gz')
            run('tar xzvf web.py-0.36.tar.gz')
#            run('tar xzvf setuptools-0.6c11.tar.gz')
            with cd('devel'):
                sudo('cp -rf * /usr/include/python2.6/')
            with cd('web.py-0.36'):
                sudo('python setup.py install')
            with settings(warn_only=True): # eksekpsi jika eksekusi command gagal
                with cd('psutil-0.4.1'):
                    sudo('python setup.py install')

#            with cd('setuptools-0.6c11'):
#                sudo('python setup.py install')
            date_now = datetime.utcnow()+timedelta(hours=7)
            sudo("date -s '%s'" % date_now)
            sudo('python monitor.py > /dev/null', pty=False) # running service slurp sebagai daemon
#            sudo('python rc.py', pty=False)
#            sudo('echo "python /home/%s/Desktop/monitoring/monitor.py\nexit 0" >> /etc/rc.local' % username)

            with settings(warn_only=True):
                sudo('chmod 777 /home/%s/Desktop/monitoring/performance.db' % username)
                sudo('chmod 777 /tmp/monitor.pid')
            sudo('''echo "#!/usr/bin/python\nimport os\nos.system('python /home/%s/Desktop/monitoring/monitor.py')" > /etc/init.d/uptmonitor''' % username) # auto create script utk injeksi service agar selalu berjalan setiap server reboot
            sudo('chmod 777 /etc/init.d/uptmonitor')
            
            with settings(warn_only=True): # eksepsi jika eksekusi command gagal
                sudo('update-rc.d -f uptmonitor remove') # remove service jika sudah tersedia sebelumnya
            sudo('update-rc.d uptmonitor defaults') # set service pada rc.d agar selalu berjalan setelah reboot
            
#            sudo('chmod 777 /home/%s/Desktop/monitoring/rest_service.py' % username)
            
#            with settings(warn_only=True):
#                sudo('chmod 777 /home/%s/Desktop/monitoring/performance.db' % username)
#                sudo('unlink /etc/init.d/uptmon')
#                sudo('unlink /etc/init.d/rest_service.py')
#                sudo('unlink /etc/init.d/performance.db')    

#            sudo('cp -rf /home/%s/Desktop/monitoring/monitor.py /etc/init.d/uptmon' % username)
#            sudo('cp -rf /home/%s/Desktop/monitoring/rest_service.py /etc/init.d/rest_service.py' % username)
#            with settings(warn_only=True):
#                sudo('cp -rf /home/%s/Desktop/monitoring/performance.db /etc/init.d/performance.db' % username)
#            sudo('chmod 777 /etc/init.d/rest_service.py')
#            with settings(warn_only=True):
#                sudo('chmod 777 /etc/init.d/performance.db')

#            with settings(warn_only=True):
#                sudo('update-rc.d -f uptmon remove')
#            sudo('update-rc.d uptmon defaults')
            
    except Exception, err:
        print err

          
def running():
    copy()
#    install()
#    slurp()

