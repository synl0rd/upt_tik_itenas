from stompy import Stomp

import psutil
import multiprocessing
import signal
import sys
import re
import commands
import json
import sched
import time

def signal_timeout(signum, frame):
    raise Exception

def schedule_monitor():
    stomp = Stomp(hostname='%s' % STOMP_SERVER_IP, port=54123)
    stomp.connect()
    stomp.subscribe({'destination':'/monitor',
                     'ack':'client'})
    memory_percent = psutil.phymem_usage().percent
    virtual_memory_percent = psutil.virtmem_usage().percent
    hdd_usage_percent = psutil.disk_usage('/').percent
    cpu_usage_percent = psutil.cpu_percent(percpu=True)
        
    if float(memory_percent) < 50:
        status_memory = 'green'
    if float(memory_percent) >= 50:
        status_memory = 'orange'
    if float(memory_percent) >= 70:
        status_memory = 'red'

    if float(virtual_memory_percent) < 50:
        status_virtual_memory = 'green'
    if float(virtual_memory_percent) >= 50:
        status_virtual_memory = 'orange'
    if float(virtual_memory_percent) >= 70:
        status_virtual_memory = 'red'

    if float(hdd_usage_percent) < 50:
        status_hdd = 'green'
    if float(hdd_usage_percent) >= 50:
        status_hdd = 'orange'
    if float(hdd_usage_percent) >= 70:
        status_hdd = 'red'

    signal.signal(signal.SIGALRM, signal_timeout)
    try:
        signal.alarm(1)
        data = {'memory':memory_percent, 
                'ip':current_ip, 
                'status_memory': status_memory,
                'status_virtual': status_virtual_memory,
                'status_hdd': status_hdd,
                'virtual_memory': virtual_memory_percent,
                'cpu': cpu_usage_percent,
                'hdd': hdd_usage_percent,
                }
        data = json.dumps(data)
        stomp.send({'destination': '/monitor',
                    'body': data,
                    'persistent': 'true'})
#        stomp.put(data, destination='/monitor')
        signal.alarm(0)
    except Exception, err:
        print err
        stomp.disconnect()
        pass

def go(s):
    for i in range(60*24):
        s.enter(60*i, 1, schedule_monitor, ())
    s.run()
    
if __name__ == '__main__':
    try:
        STOMP_SERVER_IP = open('server_ip.txt', 'r').read()
    except Exception:
        STOMP_SERVER_IP = 'localhost'

    ifconfig_result = commands.getoutput('ifconfig eth0')
    pattern = re.compile(r'inet addr:(.*)  Bcast')
    current_ip = re.search(pattern, ifconfig_result).groups()[0]
    
    s = sched.scheduler(time.time, time.sleep)
    go(s)
