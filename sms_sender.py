# sms sender read data berdasarkan argumen input (argumen pertama pesan text dan argumen kedua nomer tujuan)
import sys
import gammu

try:
    sm = gammu.StateMachine()
    sm.ReadConfig()
    sm.Init()
    sm.SendSMS({'Text':sys.argv[1], 'SMSC':{'Location':1}, 'Number': sys.argv[2]})
except Exception:
    pass
